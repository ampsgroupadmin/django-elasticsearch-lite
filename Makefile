prepare:
	@echo 'Installing dev packages ..'
	pip install setuptools==44.0.0
	pip install -e .
	pip install -r requirements.txt

run:
	python manage.py runserver 8000

clean:
	find . -type f -name '*.pyc' -delete
	find . -type f -name '*.swp' -delete

test:
	@echo 'Running tests with tox ..'
	tox

coverage:
	@echo 'Running coverage with tox ..'
	tox

check:
	@echo 'Check flake8 ..'
	flake8 --output-file=flake8.log
	@echo 'Check isort ..'
	isort -c . --skip migrations
	@echo 'Check bandit ..'
	bandit -c .bandit-config.yaml -r .

.PHONY: docs
docs:
	cd docs/ && $(MAKE) clean && $(MAKE) html
