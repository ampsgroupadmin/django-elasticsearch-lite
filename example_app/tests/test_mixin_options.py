from elasticsearch_lite import auto_index, helpers

from ..models import ExampleModel, ExampleModelChild, ModelWithFkey, \
    SingleFieldModel, SingleFieldModelWithCustomMappings
from .test_base import BaseTestCase


class TestMixinOptionsWithoutEsIndexPrefix(BaseTestCase):

    def tearDown(self):
        super(TestMixinOptionsWithoutEsIndexPrefix, self).tearDown()
        helpers._es_instance = None  # we need to clean the global variable

    def test_field_excluded(self):
        """Test ElasticSearch exclude class attribute.

        In this situation a field that has an excluded name should not be
        included in the es_fields.
        It also check if a parent link isn't included in the fields.
        """
        expected_names = ['child_field', u'id', 'name', 'parent']

        fields = ExampleModelChild._es_get_fields()

        field_names = sorted([f.name for f in fields])
        self.assertEqual(field_names, expected_names)

    def test_field_included(self):
        expected_names = ['included']

        fields = SingleFieldModel._es_get_fields()

        field_names = sorted([f.name for f in fields])
        self.assertEqual(field_names, expected_names)

    def test_foreign_keys_included(self):
        expected_names = ['fkey', u'id', 'm2m', 'name']

        fields = ModelWithFkey._es_get_fields()

        field_names = sorted([f.name for f in fields])
        self.assertEqual(field_names, expected_names)

    def test_auto_index_context_manager(self):
        instance = ExampleModel.objects.create(name="Name", count=1)

        before_value = instance.ElasticSearch.auto_index
        value_to_set = not before_value
        with auto_index(instance, value_to_set):
            managed_value = instance.ElasticSearch.auto_index
        after_value = instance.ElasticSearch.auto_index

        self.assertEquals(managed_value, value_to_set)
        self.assertEquals(after_value, before_value)

    def test_overriding_hosts_setting(self):
        hosts = [{'host': 'some_host', 'port': '9200'}]

        with self.settings(ES_HOSTS=hosts):
            es = helpers.get_es_instance(force_new=True)
            self.assertEquals(es.transport.hosts, hosts)

    def test_es_kwargs_setting_when_ES_HOSTS_not_set(self):
        class Cls(object):
            pass
        kwargs = {'connection_class': Cls}

        with self.settings(ES_KWARGS=kwargs, ES_HOSTS=None):
            es = helpers.get_es_instance(force_new=True)
            self.assertEquals(es.transport.connection_class, Cls)

    def test_es_kwargs_setting_when_ES_HOSTS_is_set(self):
        """Test if it is possible to set ES_KWARGS when ES_HOSTS is set.

        When trying to pass a custom connection_class it should at least
        define two parameters in the __init__ method: host and port
        (dictionary keys from the ES_HOSTS list).
        """
        class Cls(object):
            def __init__(self, host, port):
                self.host = host
                self.port = port
        kwargs = {'connection_class': Cls}
        hosts = [{'host': 'some_host', 'port': '9200'}]

        with self.settings(ES_KWARGS=kwargs, ES_HOSTS=hosts):
            es = helpers.get_es_instance(force_new=True)
            self.assertEquals(es.transport.connection_class, Cls)

    def test_get_registered_for_all_apps_found_registered_models(self):
        expected_model_names = [
            'ExampleModel',
            'ExampleModelChild',
            'FirstModel',
            'ModelWithFkey',
            'SingleFieldModel',
            'SingleFieldModelWithCustomMappings',
        ]

        models = helpers.get_registered()

        names = sorted([model.__name__ for model in models])
        self.assertEqual(names, expected_model_names)

    def test_get_registered_for_single_app_found_registered_models_from_app(self):  # noqa
        expected_model_names = ['FirstModel']

        app_models = helpers.get_registered('example_app2')

        names = sorted([model.__name__ for model in app_models])
        self.assertEqual(names, expected_model_names)

    def test_custom_field_in_document(self):
        instance = ExampleModel.objects.create(name="Name", count=1)
        expected_doc = {
            'count': 1,
            'name': 'Name',
            'django_model': 'ExampleModel',
            'other_name': 'SomeOtherName',
            u'id': instance.pk,
            'django_app': u'example_app',
        }

        doc = instance.es_get_doc()

        self.assertDictEqual(doc, expected_doc)

    def test_custom_index(self):
        instance = SingleFieldModel.objects.create(
            name='XXX',
            included=1,
            count=23,
        )
        expected_result = {
            'id': instance.pk,
            'index': 'other_index',
            'doc_type': 'other_type',
            'parent': '',
        }

        meta = instance.es_meta_data()

        self.assertDictEqual(meta, expected_result)

    def test_custom_id_and_parent(self):
        instance = SingleFieldModel.objects.create(
            name='XXX',
            parent='YYY',
            included=1,
            count=20,
        )
        expected_result = {
            'id': instance.pk,
            'index': 'other_index',
            'doc_type': 'other_type',
            'parent': instance.parent,
        }

        meta = instance.es_meta_data()

        self.assertDictEqual(meta, expected_result)

    def test_default_mappings(self):
        expected_maps = {
            '_parent': {'type': 'parent_class'},
            'properties': {
                'django_app': {'index': 'not_analyzed', 'type': 'string'},
                'name': {'index': 'not_analyzed', 'type': 'string'},
                'parent': {'index': 'not_analyzed', 'type': 'string'},
                'django_model': {'index': 'not_analyzed', 'type': 'string'},
            },
        }

        maps = SingleFieldModel.es_get_defined_mappings()

        self.assertDictEqual(maps, expected_maps)

    def test_custom_mappings(self):
        expected_maps = {
            '_parent': {'type': 'parent_class'},
            'properties': {'count': {'type': 'integer'}},
        }

        maps = SingleFieldModelWithCustomMappings.es_get_defined_mappings()

        self.assertDictEqual(maps, expected_maps)


class TestMixinOptionsWithEsIndexPrefix(TestMixinOptionsWithoutEsIndexPrefix):

    es_index_prefix = 'test1234'

    def test_custom_index(self):
        instance = SingleFieldModel.objects.create(
            name='XXX',
            included=1,
            count=23,
        )
        expected_result = {
            'id': instance.pk,
            'index': '{}_other_index'.format(self.es_index_prefix),
            'doc_type': 'other_type',
            'parent': '',
        }

        meta = instance.es_meta_data()

        self.assertDictEqual(meta, expected_result)

    def test_custom_id_and_parent(self):
        instance = SingleFieldModel.objects.create(
            name='ABC',
            parent='CDE',
            included=1,
            count=20,
        )
        expected_result = {
            'id': instance.pk,
            'index': '{}_other_index'.format(self.es_index_prefix),
            'doc_type': 'other_type',
            'parent': instance.parent,
        }

        meta = instance.es_meta_data()

        self.assertDictEqual(meta, expected_result)
