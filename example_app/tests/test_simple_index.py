from builtins import range
from unittest.mock import patch

from django.db import connection
from django.test.utils import override_settings

from elasticsearch2.exceptions import NotFoundError

from elasticsearch_lite import bulk_index

from ..models import ExampleModel, ExampleModelChild, ModelWithFkey, \
    SingleFieldModel
from .test_base import BaseTestCase


class TestSimpleIndexWithoutEsIndexPrefix(BaseTestCase):

    def setUp(self):
        super(TestSimpleIndexWithoutEsIndexPrefix, self).setUp()
        def _on_commit(func, using=None):
            func()
        if hasattr(connection, 'on_commit'):
            self.addCleanup(patch.stopall)
            self.m_on_commit = patch(
                'django.db.connection.on_commit', side_effect=_on_commit
            ).start()

    def tearDown(self):
        self.es.indices.delete(index=['other_index'], ignore=404)
        self.es.indices.delete(index=['example_app'], ignore=404)
        super(TestSimpleIndexWithoutEsIndexPrefix, self).tearDown()

    def is_in_index(self, pk, doc_type='examplemodel', **kwargs):
        return self.es.exists(
            index='example_app',
            doc_type=doc_type,
            id=pk,
            **kwargs
        )

    def get_data(self, pk):
        return self.es.get(index='example_app', doc_type='examplemodel', id=pk)

    def test_single_object_indexed(self):
        instance = ExampleModel.objects.create(name='SampleName', count=23)

        instance.es_index_me()

        self.assertTrue(self.is_in_index(instance.pk))

    def test_single_object_indexed_with_old_version(self):
        instance = ExampleModel.objects.create(name='SampleName', count=23)
        instance.es_index_me()
        expected_result = {
            u'_type': u'examplemodel',
            u'_source': {
                u'count': instance.count,
                u'name': str(instance.name),
                u'django_model': u'ExampleModel',
                u'other_name': u'SomeOtherName',
                u'id': instance.pk,
                u'django_app': u'example_app',
            },
            u'_index': str(self._get_index('example_app')),
            u'_version': 2,
            u'found': True,
            u'_id': str(instance.pk),
        }

        instance.es_index_me()

        result = self.get_data(instance.pk)
        self.assertDictEqual(result, expected_result)

    @patch('elasticsearch_lite.tasks.close_old_connections')
    def test_single_object_indexed_async(self, m_close_connection):
        instance = ExampleModel.objects.create(name='SampleAsync', count=23)

        instance.es_index_me_async()

        self.assertTrue(self.is_in_index(instance.pk))

    def test_delete(self):
        instance = ExampleModel.objects.create(name='ToDelete', count=23)
        instance.es_index_me()
        self.assertTrue(self.is_in_index(instance.pk))

        instance.es_delete_me()

        self.assertFalse(self.is_in_index(instance.pk), 'Present after delete')

    def test_delete_async(self):
        instance = ExampleModel.objects.create(name='ToDeleteAsync', count=23)
        instance.es_index_me()
        self.assertTrue(self.is_in_index(instance.pk))

        instance.es_delete_me_async()

        self.assertFalse(self.is_in_index(instance.pk), 'Present after delete')

    def test_bulk_indexing(self):
        instances = {}
        for x in range(10):
            instance = ExampleModel.objects.create(
                name='SampleName' + str(x),
                count=23 + x,
            )
            instances[instance.pk] = instance
        doc_index = u'{}_example_app'.format(self.es_index_prefix) if self.es_index_prefix else u'example_app'  # noqa
        expected_result = {
            u'_index': doc_index,
            u'_source': {},
            u'_type': u'examplemodel',
            u'_version': 1,
            u'found': True,
        }

        bulk_index(ExampleModel.objects.all())

        for pk, instance in instances.items():
            expected_result[u'_id'] = str(instance.pk)
            expected_result[u'_source'] = {
                u'count': instance.count,
                u'django_app': u'example_app',
                u'django_model': u'ExampleModel',
                u'id': instance.pk,
                u'name': instance.name,
                u'other_name': u'SomeOtherName',
            }
            result = self.get_data(instance.pk)
            self.assertDictEqual(result, expected_result)

    def test_mappings_installation(self):
        get_map_kwargs = dict(
            index=['example_app'], doc_type='examplemodel', fields='count'
        )
        self.es.indices.create(index=['example_app'])
        with self.assertRaises(NotFoundError):
            self.es.indices.get_field_mapping(**get_map_kwargs)

        ExampleModel.es_put_mappings(refresh=True)

        result = self.es.indices.get_field_mapping(**get_map_kwargs)
        fmap = result['example_app']['mappings']['examplemodel']
        self.assertEquals(fmap['count']['mapping']['count']['type'], 'string')

    def test_automatic_indexing_on_creation(self):
        instance = ExampleModel(name="Name", count=1)
        # disabled
        instance.save()
        self.assertFalse(self.is_in_index(instance.pk))
        with self.assertRaises(NotFoundError):
            self.es.indices.refresh(index=['example_app'])

        # enabled
        instance = ExampleModelChild(
            name='Name',
            parent=instance.pk,
            count=1,
        )
        instance.save()

        self.es.indices.refresh(index=['example_app'])
        self.assertTrue(
            self.is_in_index(
                instance.pk,
                'examplemodelchild',
                parent=instance.parent,
            ),
        )

    def test_automatic_indexing_on_deletion(self):
        instance = ExampleModelChild(
            name='Name',
            parent=123,
            count=1,
        )
        instance.save()
        pk = instance.pk

        instance.delete()

        self.assertFalse(self.is_in_index(pk), 'Present after delete')

    def test_child_with_parent_indexed(self):
        ExampleModelChild.es_put_mappings()
        ExampleModel.es_put_mappings()

        child = ExampleModelChild.objects.create(
            name='Child',
            count=1,
            parent='11234556',
        )

        self.assertTrue(child.es_indexed())

    def test_foreign_key_collected_properly(self):
        parent = SingleFieldModel.objects.create(
            name='Model1',
            count=1,
            included=1,
        )
        child = ModelWithFkey(fkey=parent, name='Child')
        expected_doc = {
            'django_app': u'example_app',
            'fkey': child.fkey.pk,
            u'id': None,
            'name': child.name,
            'django_model': 'ModelWithFkey',
        }

        doc = child.es_get_doc()

        self.assertEquals(doc, expected_doc)

    def test_m2m_field_collected_properly(self):
        ex1 = ExampleModel.objects.create(name="Model1", count=1)
        ex2 = ExampleModel.objects.create(name="Model2", count=2)
        parent = SingleFieldModel.objects.create(
            name="Model1",
            count=1,
            included=1,
        )
        child = ModelWithFkey.objects.create(name='Child', fkey=parent)
        child.m2m.add(*[ex1, ex2])
        expected_doc = {
            'name': child.name,
            'django_model': 'ModelWithFkey',
            'm2m': [ex1.pk, ex2.pk],
            'fkey': child.fkey.pk,
            u'id': child.pk,
            'django_app': u'example_app',
        }

        doc = child.es_get_doc()

        self.assertEquals(doc, expected_doc)

    def test_es_get_index(self):
        instance = SingleFieldModel.objects.create(
            name='XXX',
            parent='YYY',
            included=1,
            count=20,
        )
        expected_index = self._get_index('other_index')

        index = instance.es_get_index()

        self.assertEqual(index, expected_index)

    def test_es_update_aliases_updates_aliases(self):
        custom_es_index = self._get_index('other_index')
        mapping = {
            custom_es_index: {
                'alias_other_index': {
                    'body': {'routing': '1'},
                },
            },
        }
        instance = SingleFieldModel.objects.create(
            name='XXX',
            parent='YYY',
            included=1,
            count=20,
        )
        instance.es_put_mappings()
        expected_aliases = {
            str(custom_es_index): {
                u'aliases': {
                    u'alias_other_index': {
                        u'search_routing': u'1',
                        u'index_routing': u'1',
                    },
                },
            },
        }

        with self.settings(ES_INDEX_PREFIX=self.es_index_prefix, ES_ALIAS_MAPPING=mapping):  # noqa
            instance.es_update_aliases()

        aliases = self.es.indices.get_alias(
            index=custom_es_index,
            name='alias_other_index',
        )
        self.assertDictEqual(aliases, expected_aliases)

    @override_settings(
        ES_AUTO_INDEX_IN_CELERY=True,
    )
    @patch('elasticsearch_lite.mixins.connection')
    def test_run_index_if_backend_has_attr_on_commit_async(self, connection):
        # Arrangement
        example = ExampleModelChild(
            name='test',
            parent=1,
            count=2,
            child_field='test',
        )

        # Act
        example.save()

        # Assert
        connection.on_commit.assert_called_once_with(example.es_index_me_async)

    @patch('elasticsearch_lite.mixins.connection')
    def test_run_index_if_backend_has_attr_on_commit(self, connection):
        # Arrangement
        example = ExampleModelChild(
            name='test',
            parent=1,
            count=2,
            child_field='test',
        )

        # Act
        example.save()

        # Assert
        connection.on_commit.assert_called_once_with(example.es_index_me)


class TestSimpleIndexWithEsIndexPrefix(TestSimpleIndexWithoutEsIndexPrefix):

    es_index_prefix = 'test1234'

    def test_mappings_installation(self):
        base_index = 'example_app'
        expected_index = '{}_{}'.format(self.es_index_prefix, base_index)
        get_map_kwargs = dict(
            index=[base_index], doc_type='examplemodel', fields='count'
        )
        self.es.indices.create(index=[base_index])
        with self.assertRaises(NotFoundError):
            self.es.indices.get_field_mapping(**get_map_kwargs)

        ExampleModel.es_put_mappings(refresh=True)

        result = self.es.indices.get_field_mapping(**get_map_kwargs)
        fmap = result[expected_index]['mappings']['examplemodel']
        self.assertEquals(fmap['count']['mapping']['count']['type'], 'string')
