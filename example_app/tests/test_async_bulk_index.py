from builtins import range
from unittest.mock import patch

from django.conf import settings
from django.test import TransactionTestCase

from elasticsearch_lite import bulk, bulk_index_async
from elasticsearch_lite.es_proxy import ElasticsearchProxy

from ..models import ExampleModel


class TestAsyncBulkIndexingWithoutEsIndexPrefix(TransactionTestCase):

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it.

        We override settings in setUp because when we use the override_settings
        decorator on the class it causes problems in child classes. The child's
        decorator value will be overwritten by this class's decorator value.

        This problem was fix in Django1.8 - so when we will drop support for
        Dj<1.8 we can use class decorators instead of setUp & tearDown methods.

        The attribute name was copied from Django's 1.8 SimpleTestCase class
        attribute so we can support two versions of Django (1.6 and 1.8).
        """
        self._overridden_settings = self.settings(ES_INDEX_PREFIX='')
        self._overridden_settings.enable()

        self._prepare_es()

        patch(
            'elasticsearch_lite.bulk.get_es_instance',
            return_value=self.es,
        ).start()

    def tearDown(self):
        self.es.indices.delete(index=['example_app'], ignore=404)
        self._overridden_settings.disable()

    def _prepare_es(self):
        self.es = ElasticsearchProxy(settings.ES_HOSTS)
        self.es.indices.delete(index=['example_app'], ignore=404)
        self.es.indices.create(index=['example_app'])
        self.instances = {}
        for x in range(100):
            instance = ExampleModel.objects.create(name='SampleName' + str(x),
                                                   count=23 + x)
            self.instances[instance.pk] = instance

    def check_instances_indexed(self):
        self.es.indices.refresh(index=['example_app'])
        for pk, instance in self.instances.items():
            result = self.get_data(instance.pk)
            source = result['_source']
            self.assertEquals(source['count'], instance.count)
            self.assertEquals(source['name'], instance.name)

    def get_data(self, pk):
        return self.es.get(index='example_app', doc_type='examplemodel', id=pk)

    def test_async_bulk_indexing(self):
        bulk_index_async(ExampleModel.objects.all(), step_size=10)
        self.check_instances_indexed()

    def test_async_bulk_indexing_speedup(self):
        bulk_index_async(ExampleModel.objects.all(), step_size=10,
                         klass=bulk.SpeedupBulkIndexer)
        self.check_instances_indexed()


class TestAsyncBulkIndexingWithEsIndexPrefix(TestAsyncBulkIndexingWithoutEsIndexPrefix):  # noqa

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it.

        We override settings in setUp because when we use the override_settings
        decorator on the class it inherits the
        TestAsyncBulkIndexingWithoutEsIndexPrefix and changes the settings
        variable to the one passed on TestAsyncBulkIndexingWithoutEsIndexPrefix
        class.

        This problem was fix in Django1.8 - so when we will drop support for
        Dj<1.8 we can use class decorators instead of setUp & tearDown methods.

        The attribute name was copied from Django's 1.8 SimpleTestCase class
        attribute so we can support two versions of Django (1.6 and 1.8).
        """
        self._overridden_settings = self.settings(ES_INDEX_PREFIX='test1234')
        self._overridden_settings.enable()

        self._prepare_es()

        patch(
            'elasticsearch_lite.bulk.get_es_instance',
            return_value=self.es,
        ).start()
