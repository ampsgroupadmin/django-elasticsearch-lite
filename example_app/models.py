from django.db import models

from elasticsearch_lite.mixins import ESIndexedModelMixin


class ExampleModel(models.Model, ESIndexedModelMixin):
    name = models.CharField('Name', max_length=100)
    count = models.IntegerField('Count')

    class ElasticSearch:
        properties = {
            'name': {
                "type": "string",
                "index": "not_analyzed",
            },
            'count': {
                "type": "string",
                "index": "not_analyzed",
            }
        }
        auto_index = False

    def es_get_doc(self):
        doc = super(ExampleModel, self).es_get_doc()
        doc['other_name'] = 'SomeOtherName'
        return doc


class ExampleModelChild(ExampleModel):
    child_field = models.CharField('Child field', max_length=100)
    parent = models.IntegerField('Parent')

    class ElasticSearch:
        exclude = ('count', )
        auto_index = True
        parent_field = 'parent'
        parent_type = 'examplemodel'


class SingleFieldModel(models.Model, ESIndexedModelMixin):
    name = models.CharField('Name', max_length=100, primary_key=True)
    parent = models.CharField('Parent', max_length=100)
    count = models.IntegerField('Count')
    included = models.IntegerField('included')

    class ElasticSearch:
        include = ('included', )
        doc_index = 'other_index'
        doc_type = 'other_type'
        id_field = 'name'
        parent_field = 'parent'
        parent_type = 'parent_class'


class SingleFieldModelWithCustomMappings(models.Model, ESIndexedModelMixin):
    name = models.CharField('Name', max_length=100, primary_key=True)
    parent = models.CharField('Parent', max_length=100)
    count = models.IntegerField('Count')
    included = models.IntegerField('included')

    class ElasticSearch:
        include = ('included', )
        doc_index = 'other_index'
        doc_type = 'other_type'
        id_field = 'name'
        parent_field = 'parent'
        parent_type = 'parent_class'
        mappings = {
            'properties': {
                'count': {'type': 'integer'},
            }
        }


class ModelWithFkey(models.Model, ESIndexedModelMixin):
    name = models.CharField('Name', max_length=100)
    fkey = models.ForeignKey(SingleFieldModel, on_delete=models.CASCADE)
    m2m = models.ManyToManyField(ExampleModel)


class SomeOtherModel(models.Model):
    pass
