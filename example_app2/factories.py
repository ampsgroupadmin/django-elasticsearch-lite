import factory
from factory.fuzzy import FuzzyInteger, FuzzyText

from .models import FirstModel


class FirstModelFactory(factory.django.DjangoModelFactory):
    """A factory for FirstModel."""
    name = FuzzyText(length=100)
    count = FuzzyInteger(0, 10)

    class Meta:
        model = FirstModel
