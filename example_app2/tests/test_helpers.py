from datetime import date

from elasticsearch_lite.helpers import EmptyTimedIndicesResult, \
    get_indices_in_range

from .test_base import BaseTestCase


class GetIndicesInRangeWithoutPrefixTestCase(BaseTestCase):
    """TestCase for get_indices_in_range function."""

    def setUp(self):
        super(GetIndicesInRangeWithoutPrefixTestCase, self).setUp()
        self.expected_index = self._get_index('example')
        self.es.indices.create(index=self.expected_index)

    def tearDown(self):
        super(GetIndicesInRangeWithoutPrefixTestCase, self).tearDown()
        self.es.indices.delete(index=self.expected_index, ignore=404)
        self.es.indices.delete(index='example_*', ignore=404)

    def test_raises_error_when_empty_indices_in_range(self):
        start_date = date(2020, 1, 2)
        end_date = date(2020, 3, 1)

        with self.assertRaises(EmptyTimedIndicesResult):
            get_indices_in_range(self.expected_index, start_date, end_date)

    def test_gets_indices_in_range(self):
        start_date = date(2020, 1, 1)
        end_date = date(2020, 3, 3)
        expected_indices = [
            self._get_index('example', date(2020, 1, 1)),
            self._get_index('example', date(2020, 2, 1)),
        ]
        for index in expected_indices:
            self.es.indices.create(index=index)
            self.es.indices.refresh(index=index)

        result = get_indices_in_range(
            self.expected_index,
            start_date,
            end_date,
        )

        self.assertEqual(result, expected_indices)


class GetIndicesInRangeWithPrefixTestCase(GetIndicesInRangeWithoutPrefixTestCase):  # noqa
    """TestCase for get_indices_in_range function."""

    es_index_prefix = 'test1234'
