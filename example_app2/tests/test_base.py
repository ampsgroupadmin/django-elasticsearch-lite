from unittest.mock import patch

from django.conf import settings
from django.test import TestCase

from elasticsearch_lite.es_proxy import ElasticsearchProxy


class BaseTestCase(TestCase):
    """Base testCase for setting up a test environment."""

    es_index_prefix = ''

    def setUp(self):
        """Hook method for setting up the test fixture before exercising it.

        We override settings in setUp because when we use the override_settings
        decorator on the class it causes problems in child classes. The child's
        decorator value will be overwritten by this class's decorator value.

        This problem was fix in Django1.8 - so when we will drop support for
        Dj<1.8 we can use class decorators instead of setUp & tearDown methods.

        The attribute name was copied from Django's 1.8 SimpleTestCase class
        attribute so we can support two versions of Django (1.6 and 1.8).
        """
        self._overridden_settings = self.settings(
            ES_INDEX_PREFIX=self.es_index_prefix,
        )
        self._overridden_settings.enable()
        self.es = ElasticsearchProxy(settings.ES_HOSTS)

        patch('elasticsearch_lite.helpers._es_instance', self.es).start()
        self.addCleanup(patch.stopall)

    def tearDown(self):
        self.es.indices.delete(index=['example_*'], ignore=404)
        self._overridden_settings.disable()

    def _get_index(self, index_doc, date=None):
        """Get date index for a given model.

        Args:
            index_doc (str): a index name, for example a model's name
            date (datetime obj): a datetime.date instance from which to get the
                date
        """
        index = index_doc
        if date:
            index = '{}_{}'.format(index_doc, date.strftime('%Y-%m'))
        if self.es_index_prefix:
            return '{}_{}'.format(self.es_index_prefix, index)
        return index
