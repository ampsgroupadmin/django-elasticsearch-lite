from datetime import date, datetime
from unittest.mock import ANY

from dateutil.relativedelta import relativedelta

from elasticsearch_lite.tasks import create_month_indices, update_alias, \
    update_index_config

from ..factories import FirstModelFactory
from .test_base import BaseTestCase


class CreateMonthIndicesWithoutPrefixTestCase(BaseTestCase):
    """TestCase for create_month_indices on ESTimeIndexedModelMixin."""

    @staticmethod
    def _get_expected_mapping(expected_index):
        return {
            expected_index: {
                u'mappings': {
                    u'firstmodel': {
                        u'properties': {
                            u'django_app': {
                                u'index': u'not_analyzed',
                                u'type': u'string',
                            },
                            u'django_model': {
                                u'index': u'not_analyzed',
                                u'type': u'string',
                            },
                            u'name': {
                                u'index': u'not_analyzed',
                                u'type': u'string',
                            },
                        },
                    },
                },
            },
        }

    def test_task_creates_month_indices(self):
        index_date = date(2020, 2, 2)
        expected_index = self._get_index('example', index_date)
        expected_mapping = self._get_expected_mapping(expected_index)

        create_month_indices('example_app2', index_date)

        result = self.es.indices.get_mapping(index=expected_index)
        self.assertEqual(result, expected_mapping)

    def test_task_creates_month_indices_for_next_month_when_index_date_not_passed(self):  # noqa
        index_date = date.today() + relativedelta(months=1)
        expected_index = self._get_index('example', index_date)
        expected_mapping = self._get_expected_mapping(expected_index)

        create_month_indices('example_app2')

        result = self.es.indices.get_mapping(index=expected_index)
        self.assertEqual(result, expected_mapping)


class CreateMonthIndicesWithPrefixTestCase(CreateMonthIndicesWithoutPrefixTestCase):  # noqa
    """TestCase for create_month_indices on ESTimeIndexedModelMixin."""

    es_index_prefix = 'test1234'


class UpdateAliasWithoutPrefixTestCase(BaseTestCase):
    """TestCase for update_alias on ESTimeIndexedModelMixin."""

    def test_task_updates_alias(self):
        instance = FirstModelFactory.create()
        instance.es_put_mappings()
        index_name = self._get_index('example', instance.created)
        alias_name = self._get_index('alias_other_index')
        mapping = {
            'example_{timestamp}': {
                'alias_other_index': {
                    'timediff': [None, None],
                },
            },
        }
        expected_aliases = {
            index_name: {
                u'aliases': {
                    alias_name: {},
                },
            },
        }

        with self.settings(ES_INDEX_PREFIX=self.es_index_prefix, ES_ALIAS_MAPPING=mapping):  # noqa
            update_alias('example_app2')

        aliases = self.es.indices.get_alias(
            index=index_name,
            name=alias_name,
        )
        self.assertEqual(aliases, expected_aliases)


class UpdateAliasWithPrefixTestCase(UpdateAliasWithoutPrefixTestCase):
    """TestCase for update_alias on ESTimeIndexedModelMixin."""

    es_index_prefix = 'test1234'


class UpdateIndexConfigWithoutPrefixTestCase(BaseTestCase):
    """TestCase for update_index_config on ESTimeIndexedModelMixin."""

    def test_task_updates_index_config_when_in_range(self):
        index_date = datetime.now().date()
        expected_index = self._get_index('example', index_date)
        settings_index = self._get_index('example_{timestamp}')
        self.es.indices.create(index=expected_index)
        self.es.indices.put_settings(
            {'refresh_interval': '1s'},
            expected_index,
        )
        es_index_settings = {
            settings_index: [
                {
                    'timediff': [3, -5],
                    'optimize': {
                        'max_num_segments': 2
                    },
                    'settings': {
                        'index.routing.allocation.exclude.tag': 'strong_box',
                        'index.routing.allocation.include.tag': 'strongest_box',  # noqa
                    }
                }
            ]
        }
        expected_settings = {
            expected_index: {
                u'settings': {
                    u'index': {
                        u'creation_date': ANY,
                        u'number_of_replicas': ANY,
                        u'number_of_shards': ANY,
                        u'refresh_interval': u'1s',
                        u'routing': {
                            u'allocation': {
                                u'exclude': {u'tag': u'strong_box'},
                                u'include': {u'tag': u'strongest_box'},
                            },
                        },
                        u'uuid': ANY,
                        u'version': {u'created': ANY},
                    },
                },
            },
        }

        with self.settings(ES_INDEX_SETTINGS=es_index_settings):
            update_index_config()

        index_settings = self.es.indices.get_settings(index=expected_index)
        self.assertDictEqual(index_settings, expected_settings)


class UpdateIndexConfigWithPrefixTestCase(UpdateIndexConfigWithoutPrefixTestCase):  # noqa
    """TestCase for update_index_config on ESTimeIndexedModelMixin."""

    es_index_prefix = 'test1234'
