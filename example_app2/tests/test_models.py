from unittest.mock import patch

from elasticsearch_lite.bulk import bulk_index, bulk_index_async

from ..factories import FirstModelFactory
from ..models import FirstModel
from .test_base import BaseTestCase


class TestTimeIndexWithoutEsIndexPrefix(BaseTestCase):
    """TestCase for usage of ESTimeIndexedModelMixin."""

    def _get_data(self, index_doc, instance, date_field):
        """Get specific document.

        Args:
            index_doc (str): a index name, for example a model's name
            instance (object): a instance of a model to get information from
            date_field (str): name of the field from which the date should be
                taken
        """
        return self.es.get(
            index=self._get_index(index_doc, getattr(instance, date_field)),
            doc_type=instance._meta.model_name,
            id=instance.pk,
        )

    def test_single_object_indexed(self):
        instance = FirstModelFactory.create()
        expected_index = self._get_index('example', instance.created)

        instance.es_index_me()

        self.assertTrue(
            self.es.exists(
                index=expected_index,
                doc_type=instance._meta.model_name,
                id=instance.pk,
            ),
        )

    def test_single_object_indexed_with_old_version(self):
        instance = FirstModelFactory.create()
        instance.es_index_me()
        expected_result = {
            u'_type': instance._meta.model_name,
            u'_source': {
                u'count': instance.count,
                u'created': str(instance.created),
                u'name': str(instance.name),
                u'django_model': instance._meta.object_name,
                u'id': instance.pk,
                u'django_app': u'example_app2',
            },
            u'_index': str(self._get_index('example', instance.created)),
            u'_version': 2,
            u'found': True,
            u'_id': str(instance.pk),
        }

        instance.es_index_me()

        result = self._get_data('example', instance, 'created')
        self.assertDictEqual(result, expected_result)

    @patch('elasticsearch_lite.tasks.close_old_connections')
    def test_single_object_indexed_async(self, m_close_connection):
        instance = FirstModelFactory.create()
        expected_index = self._get_index('example', instance.created)

        instance.es_index_me_async()

        self.assertTrue(
            self.es.exists(
                index=expected_index,
                doc_type=instance._meta.model_name,
                id=instance.pk,
            ),
        )

    def test_delete(self):
        instance = FirstModelFactory.create()
        instance.es_index_me()
        expected_index = self._get_index('example', instance.created)

        instance.es_delete_me()

        self.assertFalse(
            self.es.exists(
                index=expected_index,
                doc_type=instance._meta.model_name,
                id=instance.pk,
            ),
        )

    def test_delete_async(self):
        instance = FirstModelFactory.create()
        instance.es_index_me()
        expected_index = self._get_index('example', instance.created)

        instance.es_delete_me_async()

        self.assertFalse(
            self.es.exists(
                index=expected_index,
                doc_type=instance._meta.model_name,
                id=instance.pk,
            ),
        )

    def test_bulk_indexing(self):
        first_models = FirstModelFactory.create_batch(10)
        expected_result = {
            u'_id': '',
            u'_index': '',
            u'_source': {},
            u'_type': u'firstmodel',
            u'_version': 1,
            u'found': True,
        }

        bulk_index(FirstModel.objects.all())

        self._assert_documents_equal(first_models, expected_result)

    def test_bulk_index_async(self):
        first_models = FirstModelFactory.create_batch(10)
        expected_result = {
            u'_id': '',
            u'_index': '',
            u'_source': {},
            u'_type': u'firstmodel',
            u'_version': 1,
            u'found': True,
        }

        bulk_index_async(FirstModel.objects.all())

        self._assert_documents_equal(first_models, expected_result)

    def _assert_documents_equal(self, instances, expected_result):
        """Check if documents in es exist and return expected dictionaries.

        Args:
            instances (QuerySet): a queryset of model instances
            expected_result (dict): the first level of the expected dicts
        """
        for instance in instances:
            expected_result[u'_id'] = str(instance.pk)
            expected_result[u'_index'] = self._get_index('example', instance.created)  # noqa
            expected_result[u'_source'] = {
                u'count': instance.count,
                u'created': str(instance.created),
                u'name': str(instance.name),
                u'django_model': instance._meta.object_name,
                u'id': instance.pk,
                u'django_app': u'example_app2',
            }
            result = self._get_data('example', instance, 'created')
            self.assertDictEqual(result, expected_result)

    def test_update_aliases_updates_aliases(self):
        """Test es_update_aliases if it works with timestamps."""
        instance = FirstModelFactory.create()
        index_name = self._get_index('example', instance.created)
        alias_name = self._get_index('alias_other_index')
        mapping = {
            'example_{timestamp}': {
                'alias_other_index': {
                    'timediff': [None, None],
                },
            },
        }
        instance.es_put_mappings()
        expected_aliases = {
            index_name: {
                u'aliases': {
                    alias_name: {},
                },
            },
        }

        with self.settings(ES_INDEX_PREFIX=self.es_index_prefix, ES_ALIAS_MAPPING=mapping):  # noqa
            instance.es_update_aliases()

        aliases = self.es.indices.get_alias(
            index=index_name,
            name=alias_name,
        )
        self.assertDictEqual(aliases, expected_aliases)

    def test_update_alias_updates_alias(self):
        """Test es_update_alias if it works with timestamps."""
        instance = FirstModelFactory.create()
        index_name = self._get_index('example', instance.created)
        mapping = {
            'example_{timestamp}': {
                'alias_other_index': {
                    'body': {'routing': '1'},
                    'timediff': [None, None],
                },
            },
        }
        instance.es_put_mappings()
        expected_aliases = {
            index_name: {
                u'aliases': {
                    u'alias_other_index': {
                        u'search_routing': u'1',
                        u'index_routing': u'1',
                    },
                },
            },
        }

        with self.settings(ES_INDEX_PREFIX=self.es_index_prefix, ES_ALIAS_MAPPING=mapping):  # noqa
            instance.es_update_alias(index_name)

        aliases = self.es.indices.get_alias(
            index=index_name,
            name='alias_other_index',
        )
        self.assertDictEqual(aliases, expected_aliases)


class TestTimeIndexWithEsIndexPrefix(TestTimeIndexWithoutEsIndexPrefix):

    es_index_prefix = 'test1234'
