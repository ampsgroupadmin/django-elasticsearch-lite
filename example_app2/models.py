from django.db import models

from elasticsearch_lite.mixins import ESTimeIndexedModelMixin


class FirstModel(models.Model, ESTimeIndexedModelMixin):
    name = models.CharField('Name', max_length=100)
    count = models.IntegerField('Count')
    created = models.DateField(auto_now_add=True)

    class ElasticSearch:
        default_column_index = 'created'
        column_index_map = {
            'created': 'example',
        }

    def __str__(self):
        return str(self.pk)

    def __unicode__(self):  # py2 compatibility
        return str(self.pk)
