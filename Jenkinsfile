pipeline {
    agent any

    parameters {
        string(name: 'branch', defaultValue: 'master', description: 'Branch name')
        booleanParam(name: 'CLEAR', defaultValue: true, description: 'Remove virtualenv and create new one')
        string(name: 'VENV', defaultValue: '../venvs/django-elasticsearch-lite', description: 'virtualenv name')
    }
    environment {
        PATH = "$VENV/bin/:$PATH"
        DJELASTICSEARCH_COMMIT_ID = sh(script:'git rev-parse origin/$branch', , returnStdout: true).trim()
        DJELASTICSEARCH_MYSQL_USER = credentials('jenkins-mysql-user')
        DJELASTICSEARCH_MYSQL_PASSWORD = credentials('jenkins-mysql-password')
        DJELASTICSEARCH_MYSQL_HOST = credentials('jenkins-mysql-host')
        DJELASTICSEARCH_MYSQL_DBNAME = credentials('django-elasticsearch-lite-db-name')
        DJELASTICSEARCH_ELASTICSEARCH_HOST = credentials('jenkins-elasticsearch-host')
        DJELASTICSEARCH_ELASTICSEARCH_PORT = credentials('jenkins-elasticsearch-port')
    }

    stages {
        stage('Prepare') {

            steps {
                bitbucketStatusNotify(buildState: "INPROGRESS", repoSlug: 'django-elasticsearch-lite', commitId: "${env.DJELASTICSEARCH_COMMIT_ID}")

                script {
                    currentBuild.displayName = "${branch} build #${BUILD_NUMBER}"
                }
                git branch: '${branch}', credentialsId: '4c146d29-1c4f-4d76-be95-dfee2bdfd738', url: 'ssh://git@bitbucket.org/ampsgroupadmin/django-elasticsearch-lite.git'
                sh 'if $CLEAR; then rm -rf $VENV; fi'
                sh 'if $CLEAR; then virtualenv --python=python3.7 --no-setuptools $VENV; fi'
                sh 'make prepare'
                sh 'make clean'
            }
        }
        stage('Check') {
            steps {
                sh 'make check'
            }
        }
        stage('Test') {
            steps {
                sh 'make coverage'
                sh 'coverage xml'
                sh 'rm -rf .tox'
            }
            post{
                always{
                    step([$class: 'CoberturaPublisher',
                                   autoUpdateHealth: false,
                                   autoUpdateStability: false,
                                   coberturaReportFile: 'coverage.xml',
                                   failNoReports: false,
                                   failUnhealthy: false,
                                   failUnstable: false,
                                   maxNumberOfBuilds: 10,
                                   onlyStable: false,
                                   sourceEncoding: 'ASCII',
                                   zoomCoverageChart: false])
                }
            }
        }
        stage('Upload') {
            when {
                anyOf {
                    changeset '.bumpversion.cfg'
                }
            }
            steps {
                echo 'Upload package ..'
                sh "python setup.py sdist upload -r mypypi"
            }
        }
    }
    post {
        success {
            bitbucketStatusNotify(buildState: "SUCCESSFUL", repoSlug: 'django-elasticsearch-lite', commitId: "${env.DJELASTICSEARCH_COMMIT_ID}")
            sh 'rm -rf $VENV'
        }

        failure {
            bitbucketStatusNotify(buildState: "FAILED", repoSlug: 'django-elasticsearch-lite', commitId: "${env.DJELASTICSEARCH_COMMIT_ID}")
            sh 'rm -rf $VENV'
        }
    }
}
