import os
import sys

from setuptools import find_packages, setup

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

python_version = sys.version_info
python_version = '{}.{}'.format(python_version.major, python_version.minor)

install_requires = [
    'Django>=3.2.13',
    'celery==4.2.2',
    'elasticsearch2==2.5.0',
]

setup(
    name='django-elasticsearch-lite',
    version='0.2.3',
    python_requires='>=3.7, <4',  # noqa
    packages=find_packages(exclude=['*.swp']),
    install_requires=install_requires,
    include_package_data=True,
    license='Not open source',
    description='Simple elasticsearch indexing for your django models',
    url='https://www.beefee.co.uk/',
    author='BeeFee',
    author_email='programmers@beefee.co.uk',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
    ],
)
