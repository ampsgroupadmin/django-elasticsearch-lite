from django.conf import settings

DEFAULTS = {
    'DB_MASTER_ALIAS': 'default',
    'ES_HOSTS': None,
    'ES_KWARGS': {},
    'ES_AUTO_INDEX': True,
    'ES_AUTO_INDEX_IN_CELERY': False,
    'ES_BULK_SEGMENT_SIZE': 50,
    'ES_FIELD_TYPE_MAPPING': {
        'CharField': {
            "type": "string",
            "index": "not_analyzed",
        },
        'DateTimeField': {
            "type": "date",
            "index": "not_analyzed",
        },
        'DecimalField': {
            "type": "double",
            "index": "not_analyzed",
        }
    },
    'ES_BULK_CLASS': None,
    'ES_MAPPING_INDEX_BATCH_SIZE': 12,

    'ES_ALIAS_MAPPING': {
        "transactions_{timestamp}": {
            "all_transactions": {
                'timediff': [None, None]
            },
            'transactions_last_12': {
                'timediff': [12, None]
            }
        },
        "bonuses_{timestamp}": {
            "all_bonuses": {
                "timediff": [None, None]
            }
        },
        "payments_{timestamp}": {
            "all_payments": {
                'timediff': [None, None],
            },
        }
    },

    'ES_INDEX_PREFIX': '',

    'ES_INDEX_SETTINGS': {
        # 'payments_{timestamp}': [
        #     {
        #         'timediff': [3, -5],
        #         'optimize': {
        #             "max_num_segments": 2
        #         },
        #         'settings': {
        #             "index.routing.allocation.exclude.tag": "strong_box",
        #             "index.routing.allocation.include.tag": "strongest_box",
        #         }
        #     }
        # ]
    }
}


def get_setting(name):
    """Poor version of an AppConf.

    Args:
        name (str): a settings variable name

    Returns:
        a value of the settings's variable or a default

    """
    return getattr(settings, name, DEFAULTS[name])
