from contextlib import contextmanager

from .bulk import bulk_index, bulk_index_async  # noqa
from .mixins import ESIndexedModelMixin  # noqa
from .module_settings import get_setting

__version__ = '0.2.3'


@contextmanager
def auto_index(instance, enabled):
    """
    Temporarily enable/disable auto_index value for given instance
    """
    default = get_setting('ES_AUTO_INDEX')
    es_cls = getattr(instance, 'ElasticSearch', None)
    prv_value = getattr(es_cls, 'auto_index', default)
    es_cls and setattr(es_cls, 'auto_index', enabled)
    yield
    es_cls and setattr(es_cls, 'auto_index', prv_value)
