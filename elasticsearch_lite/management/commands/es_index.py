# -*- coding: utf-8 -*-
import datetime

from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from dateutil.relativedelta import relativedelta
import pytz

from elasticsearch_lite import bulk_index_async
from elasticsearch_lite.helpers import get_registered
from elasticsearch_lite.module_settings import get_setting


class Command(BaseCommand):
    help = "Index selected models (TIME ENABLED)"
    args = "<app_name> <model_name>"

    def add_arguments(self, parser):
        parser.add_argument(
            '--filter',
            action='store',
            dest='filter',
            default=None,
            help='comma-separated keyword arguments for filter method '
            ' , eg. --where "is_active=True,type=XXX"',
        )
        parser.add_argument(
            '--exclude',
            action='store',
            dest='exclude',
            default=None,
            help='comma-separated keyword arguments for exlude method '
            ' , eg. --where "is_active=True,type=XXX"',
        )
        parser.add_argument(
            '--class',
            action='store',
            dest='class',
            default=None,
            help='Use custom BulkIndexer class.',
        )
        parser.add_argument(
            '--limit',
            action='store',
            type=int,
            dest='limit',
            default=None,
            help='limit number of indexed objects',
        )
        parser.add_argument(
            '--offset',
            action='store',
            type=int,
            dest='offset',
            default=None,
            help='DB select offset',
        )
        parser.add_argument(
            '--date-from',
            dest='date_from',
            action='store',
            default=None,
            help='Limit time indexed models to a date range. '
            'Can be used with --all.',
        )
        parser.add_argument(
            '--date-to',
            dest='date_to',
            action='store',
            default=None,
            help='Limit time indexed models to a date range. '
            'Can be used with --all.',
        )
        parser.add_argument(
            '--all',
            action='store_true',
            dest='all',
            default=False,
            help='Index all models with their default queryset'
            '(conflicts with app/model arguments and filtering'
            'options)',
        )

    @staticmethod
    def _get_kwargs(self, kwargs_str):
        return dict(x.split('=') for x in ','.split(kwargs_str))

    def handle(self, *args, **options):
        # Get arguments
        if options['all'] and args:
            raise CommandError('--all options conflicts with model selection')
        elif options['all']:
            models = [m for m in get_registered()]
        elif len(args) < 1:
            raise CommandError('App and model not specified')
        elif len(args) < 2:
            models = [m._meta.object_name for m in get_registered(args[0])]
            raise CommandError(
                'Model not specified. Select one of: %s' % ', '.join(models)
            )
        else:
            app_label, model_name = args[:2]
            models = [apps.get_model(app_label, model_name)]

        date_from = options['date_from']
        date_to = options['date_to']
        if date_to and not date_from:
            raise CommandError(
                'If you want to use date-to, you must provide date-from')
        if date_from:
            options['date_from'] = datetime.datetime.strptime(
                date_from, "%Y-%m-%d").replace(tzinfo=pytz.utc)
            if date_to:
                options['date_to'] = datetime.datetime.strptime(
                    date_to, "%Y-%m-%d").replace(tzinfo=pytz.utc)
            else:
                options['date_to'] = options['date_from'] + \
                    relativedelta(months=1)

        # Load custom BulkIndexer class
        bulk_cls = None
        cls_name = options['class'] or get_setting('ES_BULK_CLASS')

        # We cannot use the SpeedupBulkIndexer when using filters or limits,
        # because it ignores them.
        if not options["all"] or options["date_from"] or options["date_from"]:
            cls_name = 'elasticsearch_lite.bulk.BulkIndexer'

        self.stdout.write("Using indexer: %s" % cls_name)

        if cls_name:
            try:
                klass_list = cls_name.split('.')
                klass_name = klass_list.pop()
                module_path = '.'.join(klass_list)
                module = __import__(module_path, globals(), locals(),
                                    [klass_name])
                bulk_cls = getattr(module, klass_name)
            except (ImportError, AttributeError, IndexError):
                raise CommandError('Could not import %s' % cls_name)

        # Index models
        len_models = len(models)
        for num, model in enumerate(models, 1):
            if len_models > 1:
                self.stdout.write("Indexing %s (%d of %d)" %
                                  (model._meta.object_name, num, len_models))
            self.index_single_model(model, bulk_cls, options)

    def index_single_model(self, model, bulk_cls, options):
        qset = model.objects.all().order_by('id')
        qset = self.apply_date_filter(qset, options)

        if not options['all']:
            qset = self.apply_filters(qset, options)

        bulk_index_async(qset, klass=bulk_cls)

    def apply_date_filter(self, qset, options):
        model = qset.model

        if getattr(model, "_ELASTICSEARCH_TIME_INDEXED", False) and \
                options['date_from'] and options['date_to']:

            date_from = options['date_from']
            date_to = options['date_to']

            columns = model._es_get_option('column_index_map')

            filter_not_null = None
            filter_dates = None

            for column in columns:
                column_filter = Q(**{column + "__isnull": False})
                filter_not_null = filter_not_null & column_filter \
                    if filter_not_null else column_filter

            for column in columns:
                column_filter = Q(**{column + "__range": [date_from, date_to]})
                filter_dates = filter_dates | column_filter \
                    if filter_dates else column_filter

            qset = qset.filter(filter_not_null & filter_dates)

        return qset

    def apply_filters(self, qset, options):
        if options['filter']:
            kwargs = self._get_kwargs(options['filter'])
            qset = qset.filter(kwargs)

        if options['exclude']:
            kwargs = self._get_kwargs(options['exclude'])
            qset = qset.exclude(kwargs)

        if options['limit'] and options['offset']:
            qset = qset[options['offset']:options['offset'] + options['limit']]
        if options['limit']:
            qset = qset[:options['limit']]
        if options['offset']:
            qset = qset[options['offset']:]

        return qset
