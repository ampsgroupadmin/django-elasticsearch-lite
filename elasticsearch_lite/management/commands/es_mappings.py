# -*- coding: utf-8 -*-
import datetime
import json

from django.core.management.base import BaseCommand, CommandError

import pytz

from elasticsearch_lite.helpers import get_registered


class Command(BaseCommand):
    help = "List and install Elasticsearch mapings for models"
    args = "<app_name>"

    def add_arguments(self, parser):
        parser.add_argument(
            '--show-local',
            action='store_true',
            dest='show',
            default=True,
            help='show mappings defined for models',
        )
        parser.add_argument(
            '--install',
            action='store_true',
            dest='install',
            default=False,
            help='install mappings defined for model',
        )
        parser.add_argument(
            '--date-from',
            action='store',
            default=None,
            dest='date_from',
            help='Date range start. Only matters for monthly indices.',
        )
        parser.add_argument(
            '--date-to',
            action='store',
            default=None,
            dest='date_to',
            help='Date range end. Only matters for monthly indices. If'
            'not given, the default is to create an index for 1'
            'month',
        )

    def handle(self, *args, **options):
        date_from = options['date_from']
        date_to = options['date_to']

        if date_to and not date_from:
            raise CommandError(
                'If you want to use date-to, you must provide date-from'
            )

        if options['install'] and date_from:
            if not date_to:
                date_to = str(date_from)

            date_from = datetime.datetime.strptime(
                date_from, "%Y-%m-%d").replace(tzinfo=pytz.utc)
            date_to = datetime.datetime.strptime(
                date_to, "%Y-%m-%d").replace(tzinfo=pytz.utc)

        app_label = args[0] if args else None
        models = get_registered(app_label)
        with_mappings = [m for m in models if m.es_get_defined_mappings()]

        for model in with_mappings:
            model_name = getattr(model._meta, 'model_name', None)
            model_name = model_name or getattr(model._meta, 'module_name')
            self.stdout.write(
                'Model %s.%s' % (model._meta.app_label, model_name)
            )
            if options['install']:
                if getattr(model, "_ELASTICSEARCH_TIME_INDEXED", False) and \
                        date_from:
                    model.es_put_mappings(date_from=date_from, date_to=date_to)
                else:
                    model.es_put_mappings()
            elif options['show']:
                mappings = model.es_get_defined_mappings()
                self.stdout.write(json.dumps(mappings))
