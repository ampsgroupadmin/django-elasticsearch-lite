from __future__ import absolute_import

# Index building
from datetime import date

from django.apps import apps
from django.db import close_old_connections

from celery import shared_task
from celery.utils.log import get_task_logger
from dateutil.relativedelta import relativedelta

from .helpers import get_es_instance, get_index_date, get_registered, \
    index_in_date_range
from .module_settings import get_setting

logger = get_task_logger(__name__)


@shared_task(
    bind=True, default_retry_delay=2 * 60, max_retries=10, acks_late=True)
def save_document(self, app_label, model_name, obj_id):
    try:
        logger.debug(
            "Model_operation with: %s %s %s", app_label, model_name, obj_id)
        model = apps.get_model(app_label, model_name)
        try:
            model.es_versioned_index_object(obj_id)
        except Exception as e:
            self.retry(exc=e)
    finally:
        close_old_connections()


@shared_task(bind=True, default_retry_delay=2 * 60, max_retries=10,
             acks_late=True)
def delete_document(self, app_label, model_name, doc_info=None):
    try:
        logger.debug("Delete document: %s %s %s", app_label, model_name,
                     doc_info)
        model = apps.get_model(app_label, model_name)
        model.es_delete_by_id(
            doc_index=doc_info['index'],
            doc_type=doc_info['doc_type'],
            obj_id=doc_info['id'],
        )
    except Exception as e:
        logger.exception("model_operation failed with: %s %s %s",
                         app_label, model_name, doc_info)
        self.retry(exc=e)


@shared_task(bind=True, default_retry_delay=2 * 60, max_retries=10,
             acks_late=True)
def update_index_config(self):
    try:
        def apply_index_settings(index, index_settings):
            if index_settings.get("settings"):
                es.indices.put_settings(index_settings["settings"], index)
            if index_settings.get("optimize"):
                es.indices.optimize(index, **index_settings["optimize"])

        es = get_es_instance()
        indices = es.indices.stats(metric="indices").get('indices').keys()

        # Get all allocations setting. This tells us how to
        # allocate shards and optimize indices.
        all_allocations = get_setting("ES_INDEX_SETTINGS")

        for index in indices:
            index_name, index_date = get_index_date(index)

            # This means we have a dated index
            if index_date:
                # Get all index settings for this group of dated indices.
                all_index_settings = all_allocations.get(
                    index_name + "_{timestamp}")

                if not all_index_settings:
                    continue

                # Find the right settings for this dated index.
                for date_index_settings in all_index_settings:
                    if not index_in_date_range(
                            index_date, *date_index_settings["timediff"]):
                        continue
                    apply_index_settings(index, date_index_settings)

            else:
                index_settings = all_allocations.get(index_name)
                if not index_settings:
                    continue
                apply_index_settings(index, index_settings)
    except Exception as e:
        logger.exception("updating index config failed")
        self.retry(exc=e)


@shared_task(bind=True, default_retry_delay=2 * 60, max_retries=10,
             acks_late=True)
def create_month_indices(self, app_label=None, index_date=None):
    if index_date is None:
        index_date = date.today() + relativedelta(months=1)

    try:
        models = get_registered(app_label, "time")
        with_mappings = [m for m in models if m.es_get_defined_mappings()]

        for model in with_mappings:
            # We need to put both, because otherwise date_to will
            # become "today"
            model.es_put_mappings(date_from=index_date, date_to=index_date)
    except Exception as e:
        logger.exception("creating index for month %s failed", index_date)
        self.retry(exc=e)


@shared_task(bind=True, default_retry_delay=2 * 60, max_retries=10,
             acks_late=True)
def update_alias(self, app_label=None):
    try:
        models = get_registered(app_label, "time")
        for model in models:
            model.es_update_aliases()
    except Exception as e:
        logger.exception("updating aliases for app %s failed", app_label)
        self.retry(exc=e)
