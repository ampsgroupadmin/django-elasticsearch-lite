from builtins import range
import json
import logging

from django.conf import settings
from django.db import connection
from django.db.models.fields import NOT_PROVIDED
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from dateutil.rrule import MONTHLY, rrule
from elasticsearch2.exceptions import NotFoundError, RequestError

from .es_proxy import apply_alias_prefix, apply_index_prefix
from .helpers import format_timed_index, get_es_instance, \
    get_existing_column_indices, get_index_date, index_in_date_range
from .module_settings import get_setting
from .tasks import delete_document, save_document

logger = logging.getLogger('elasticsearch_lite.mixins')


class ESIndexedModelMixin(object):
    # It's here so we could easily find models to index without traversing
    # whole class hierarchy
    _ELASTICSEARCH_ENABLED = True

    @classmethod
    def _es_get_option(cls, param, default=None):
        """ Get ES settings parameter for this model from its meta class.

        Args:
            param (str): parameter of interest
            default: default value of the parameter if not found

        Returns:
            Parameter value or default, if the parameter was not found.
        """
        cls_meta = getattr(cls, 'ElasticSearch', None)
        if not cls_meta:
            return default
        return getattr(cls_meta, param, default)

    @classmethod
    def _es_get_fields(cls):
        """ Get the ES fields for this model. Ignores the fields marked as
        "parent_links".

        Returns:
            list: List of fields
        """
        exclude = cls._es_get_option('exclude', [])
        include = cls._es_get_option('include', [])
        all_fields = cls._meta.fields + cls._meta.many_to_many
        fields = []
        for field in all_fields:
            is_excluded = (field.remote_field and
                           hasattr(field.remote_field, 'parent_link') and
                           field.remote_field.parent_link) or\
                          field.name in exclude
            if is_excluded:
                continue

            if include and field.name not in include:
                continue

            fields.append(field)

        return fields

    @classmethod
    def es_get_index(cls):
        """ Shorthand to get the index of a model from its meta data

        Returns:
            str: Index name
        """
        index = cls._es_get_option('doc_index')
        if index:
            index = apply_index_prefix(index)

        return index

    @classmethod
    def es_get_defined_mappings(cls):
        """ Get mappings defined in ElasticSearch class.
        It uses the ES_FIELD_TYPE_MAPPING setting to set default mappings
        for fields.

        It's possible to add custom properties by adding them as a "properties"
        parameter of the ElasticSearch class, as well as custom options, using
        the "mappings" property.

        Parent type property allows you to create a parent-child relationship.

        Returns:
            dict: The mappings definition that you can then send to ES
        """
        properties = {
            'django_model': {
                "type": "string",
                "index": "not_analyzed",
            },
            'django_app': {
                "type": "string",
                "index": "not_analyzed",
            },
        }
        # Add default mappings for field types
        type_map = settings.ES_FIELD_TYPE_MAPPING
        for field in cls._meta.fields:
            field_cls = field.__class__.__name__
            if field_cls in type_map:
                properties[field.name] = type_map[field_cls]

        # Custom Model-level field properties
        custom = cls._es_get_option('properties', {})
        properties.update(custom)

        retval = {
            'properties': properties,
        }

        # Install mapping for parent type if specified
        parent_type = cls._es_get_option('parent_type')
        if parent_type:
            retval['_parent'] = {"type": parent_type}

        # Include document level options (mappings)
        options = cls._es_get_option('mappings')
        if options:
            retval.update(options)

        return retval

    @classmethod
    def es_put_mappings(cls, refresh=True):
        """
        Install mappings defined in cls.Elasticsearch.mappings.

        If the index doesn't already exist, it will be created automatically.
        New mappings can be installed for existing indices with existing
        mappings, given that the new ones don't modify the old ones. That is,
        adding new mappings is allowed, but not modifying or removing old ones.

        Args:
            refresh: Whether to refresh the index
        """
        maps = cls.es_get_defined_mappings()

        if maps:
            meta = cls().es_meta_data()
            body = {
                meta['doc_type']: maps
            }

            es = get_es_instance()

            if not es.indices.exists(index=meta['index']):
                es.indices.create(index=meta['index'])

            es.indices.put_mapping(
                index=meta['index'], doc_type=meta['doc_type'], body=body)
            refresh and es.indices.refresh(index=[meta['index']])

            cls.es_update_aliases()

    @classmethod
    def es_update_aliases(cls):
        """Update aliases associated with this model's index."""
        es = get_es_instance()

        # Get the alias mapping.
        alias_mapping = get_setting('ES_ALIAS_MAPPING')
        alias_mapping = apply_alias_prefix(alias_mapping)

        index = cls.es_get_index()
        if index not in alias_mapping:
            return
        index_aliases = alias_mapping[index]
        for alias_name, alias_settings in index_aliases.items():
            # Get the parameters for this alias
            body = json.dumps(alias_settings.get("body", {}))
            es.indices.put_alias(name=alias_name, index=index, body=body)

    @classmethod
    def es_class_meta_data(cls):
        doc_index = cls._es_get_option('doc_index') or cls._meta.app_label
        doc_index = apply_index_prefix(doc_index)

        model_name = getattr(cls._meta, 'model_name', None)
        model_name = model_name or getattr(cls._meta, 'module_name')

        doc_type = cls._es_get_option('doc_type') or model_name
        return doc_index, doc_type

    @classmethod
    def es_meta_include_old_version(cls, meta):
        es = get_es_instance()
        try:
            old_doc = es.get(_source=False, **meta)
            meta['version'] = old_doc['_version']
        except NotFoundError:
            pass

    @classmethod
    def es_delete_by_id(cls, doc_index, doc_type, obj_id):
        """Delete a document using it's pk value.

        Args:
            doc_index (str): the name of the index
            doc_type (str): the type of the document
            obj_id (str): the document id
        """
        es = get_es_instance()
        es.delete(doc_index, doc_type, obj_id)

    @classmethod
    def es_check_mapping(cls, index, doc_type):
        es = get_es_instance()
        if not es.indices.exists_type(index, doc_type):
            cls.es_put_mappings()

    @classmethod
    def es_versioned_metas(cls, obj_id):
        index, doc_type = cls.es_class_meta_data()
        # check if index exists and create when needed
        cls.es_check_mapping(index, doc_type)
        # compose meta
        meta = {'index': index, 'doc_type': doc_type, 'id': obj_id}
        # check version
        cls.es_meta_include_old_version(meta)
        return [meta]

    @classmethod
    def es_versioned_index_object(cls, obj_id):
        es = get_es_instance()
        metas = cls.es_versioned_metas(obj_id)
        db_alias = get_setting('DB_MASTER_ALIAS')
        # get current document
        instance = cls.objects.using(db_alias).get(pk=obj_id)
        doc = instance.es_get_doc()
        # try to index document
        for meta in metas:
            es.index(body=doc, **meta)

    def es_get_doc(self):
        """
        Create document from basic model fields
        Related fields are not included.
        """
        data = {
            'django_model': self._meta.object_name,
            'django_app': self._meta.app_label
        }
        fields = self.__class__._es_get_fields()
        for field in fields:
            if hasattr(field, 'm2m_reverse_name'):
                try:
                    manager = getattr(self, field.get_attname())
                except ValueError:
                    value = NOT_PROVIDED
                else:
                    value = list(manager.values_list(
                        field.m2m_reverse_target_field_name(), flat=True
                    ))
            else:
                value = getattr(self, field.get_attname(), field.default)

            if value != NOT_PROVIDED:
                data[field.name] = value

        return data

    def es_bulk_keys(self, retval):
        # Convert keys for elasticsearch.helpers.bulk
        mapping = {
            'doc_type': '_type',
        }

        # Adds an underscore to all keys. That's all it does.
        retval = dict(
            (mapping.get(k, '_' + k), v) for k, v in retval.items()
        )

        return retval

    def es_meta_data(self, bulk_keys=False):
        """ Get ElasticSearch meta data for this model.

        Args:
            bulk_keys (bool): Whether to return in a format applicable to
                bulk indexing

        Returns:
            dict: ElasticSearch meta data.
        """
        cls = self.__class__
        doc_index, doc_type = cls.es_class_meta_data()
        retval = {
            'index': doc_index,
            'doc_type': doc_type,
            'id': self.pk,
        }
        # Instance level meta data
        parent_field = cls._es_get_option('parent_field')
        if parent_field:
            retval['parent'] = getattr(self, parent_field)

        if bulk_keys:
            return self.es_bulk_keys(retval)

        return retval

    def es_index_me(self):
        """
        Add/Update this instance's info to elasticsearch index
        """
        cls = self.__class__
        es = get_es_instance()
        doc = self.es_get_doc()
        meta = self.es_meta_data()

        parent_field = cls._es_get_option('parent_field')
        if parent_field:
            meta['parent'] = getattr(self, parent_field)

        if not es.indices.exists_type(meta['index'], meta['doc_type']):
            cls.es_put_mappings()
        cls.es_meta_include_old_version(meta)
        result = es.index(body=doc, **meta)
        return result

    def es_index_me_async(self):
        """
        Add/Update this instance's info to elasticsearch index using celery
        """
        save_document.delay(
            self._meta.app_label, self._meta.object_name, self.pk
        )

    def es_delete_me(self):
        """
        Delete instance's info from elasticsearch index
        """
        es = get_es_instance()
        meta = self.es_meta_data()

        cls = self.__class__
        parent_field = cls._es_get_option('parent_field')
        if parent_field:
            meta['parent'] = getattr(self, parent_field)

        result = es.delete(**meta)
        return result

    def es_delete_me_async(self):
        """
        Delete instance's info from elasticsearch index using celery
        """
        meta = self.es_meta_data()
        delete_document.delay(
            self._meta.app_label,
            self._meta.object_name,
            doc_info=meta,
        )

    def es_indexed(self):
        """True if object is stored in elasticsearch index."""
        es = get_es_instance()
        kwargs = self.es_meta_data()

        cls = self.__class__
        parent_field = cls._es_get_option('parent_field')
        if parent_field:
            kwargs['parent'] = getattr(self, parent_field)

        return es.exists(**kwargs)


class ESTimeIndexedModelMixin(ESIndexedModelMixin):
    # Just like _ELASTICSEARCH_ENABLED, this makes it easy to find
    # the models we need. In this case, the ones that use time indexing.
    _ELASTICSEARCH_TIME_INDEXED = True

    @classmethod
    def _es_get_column_index(cls, column):
        """ Get the index name associated with a particular date column.
        This allows us to easily map a single model to multiple indices when
        it has more than one date/datetime column. For instance, we can use one
        index to group (monthly) documents by start_date and another to group
        them by end_date.

        Args:
            column: column name

        Returns:
            str: index root name
        """
        return cls._es_get_option('column_index_map')[column]

    @classmethod
    def es_get_index_range(cls, column, oldest=None, newest=None):
        """Get all date indices for this column within the given date range,
        or just spanning all records of this model.

        Uses the es_get_records_date_range function to find the appropriate
        range of dates.

        Args:
            column (str): the column we're interested in
            oldest (date): range start
            newest (date): range end

        Returns:
            list: List of date index names of form "<index_root>_%Y-%m"
        """
        obj = cls()

        # Return value containing the date indices mapped
        # to their respective date columns
        date_indices = []

        # Get date range for a specific column
        date_range = cls.es_get_records_date_range(column, oldest, newest)

        if None in date_range:
            return []

        # Loop monthly over the range
        for dt in rrule(MONTHLY, dtstart=date_range[0].replace(day=1),
                        until=date_range[1].replace(day=3)):
            index_date = dt.date().replace(day=1)
            timed_index = obj.es_get_index(column, index_date)

            # Failsafe to prevent duplicated documents.
            if timed_index in date_indices:
                continue

            date_indices.append(timed_index)

        return date_indices

    @classmethod
    def es_get_records_date_range(cls, column, oldest=None, newest=None):
        """ Get the range of dates for this column, taking into consideration
        the table contents.

        If either date is not given, we search the database for the newest,
        oldest, or both dates.

        Args:
            column (str): column name
            oldest (date): range start
            newest (date): range end

        Returns:
            tuple: Range star and end dates
        """
        if not cls.objects.exists():
            return oldest, newest

        if not oldest:
            # Without [:1] you get a limit-less query! Never forget.
            oldest_obj = cls.objects.filter(
                **{column + '__isnull': False}).order_by(column)[:1][0]
            oldest = getattr(oldest_obj, column, None)

        if not newest:
            newest_obj = cls.objects.filter(
                **{column + '__isnull': False}).order_by("-" + column)[:1][0]
            newest = getattr(newest_obj, column, None)

        return oldest, newest

    @classmethod
    def es_put_mappings(cls, refresh=True, date_from=None, date_to=None):
        """ Insert mappings for this model into dated indices within the
        specified date range. If no range is given, dates are extracted from
        the database using es_get_records_date_range.

        Once finished, this function also updates the aliases for this model.

        Args:
            refresh (bool): whether to refresh the index
            date_from (date): range start
            date_to (date): range end
        """
        maps = cls.es_get_defined_mappings()
        if not maps:
            return

        es = get_es_instance()

        meta = cls().es_meta_data()
        doctype = meta['doc_type']
        body = {doctype: maps}

        all_timed_indices = []

        # Get the column index map.
        column_index_map = cls._es_get_option('column_index_map')
        for column in column_index_map:
            timed_indices = cls.es_get_index_range(
                column, oldest=date_from, newest=date_to)
            all_timed_indices += timed_indices

            for timed_index in timed_indices:
                if not es.indices.exists(index=timed_index):
                    es.indices.create(index=timed_index)
        if all_timed_indices:

            batch_size = get_setting('ES_MAPPING_INDEX_BATCH_SIZE')
            for i in range(0, len(all_timed_indices), batch_size):
                index_batch = all_timed_indices[i:i + batch_size]
                es.indices.put_mapping(
                    index=index_batch, doc_type=doctype, body=body)
                refresh and es.indices.refresh(index=index_batch)
        cls.es_update_aliases()

    @classmethod
    def es_update_alias(cls, timed_index):
        """ Update alias for a single timed_index.
        This function can be considered deprecated, as it has a major flaw that
        is addressed by es_update_aliases - it doesn't clear aliases for timed
        indices that fall outside of the specified date range, unless you
        explicitely call it for those aliases.

        Args:
            timed_index (str): name of the timed index to update.
        """
        es = get_es_instance()

        index_root, index_date = get_index_date(timed_index)

        # Get the alias mapping.
        alias_mapping = get_setting('ES_ALIAS_MAPPING')
        alias_mapping = apply_alias_prefix(alias_mapping)
        index_key = index_root + "_{timestamp}"
        index_key = apply_index_prefix(index_key)

        if index_key not in alias_mapping:
            return

        index_aliases = alias_mapping[index_key]
        for alias_name, alias_settings in index_aliases.items():
            # Get the parameters for this alias
            body = json.dumps(alias_settings.get("body", {}))

            # Create alias if the time condition is met
            if index_in_date_range(index_date, *alias_settings["timediff"]):
                es.indices.put_alias(
                    index=timed_index, name=alias_name, body=body)
            else:
                if es.indices.exists_alias(timed_index, alias_name):
                    es.indices.delete_alias(timed_index, alias_name)

    @classmethod
    def es_update_aliases(cls):
        """ Update all aliases applicable to this model.
        Uses a single, atomic operation on ElasticSearch.
        """
        es = get_es_instance()
        alias_mapping = get_setting('ES_ALIAS_MAPPING')
        alias_mapping = apply_alias_prefix(alias_mapping)
        actions = []

        column_index_map = cls._es_get_option('column_index_map')
        columns = column_index_map.keys()
        for column in columns:
            index_root = column_index_map[column]
            index_root = apply_index_prefix(index_root)
            existing_column_indices = get_existing_column_indices(index_root)
            index_key = index_root + "_{timestamp}"
            index_aliases = alias_mapping.get(index_key)
            if not index_aliases:
                continue
            for column_index in existing_column_indices:
                index_date = get_index_date(column_index)[1]
                if index_date is None:
                    continue
                for alias_name, alias_settings in index_aliases.items():
                    alias_name = apply_index_prefix(alias_name)
                    belongs_to_alias = index_in_date_range(
                        index_date, *alias_settings["timediff"])
                    action = 'add' if belongs_to_alias else 'remove'
                    actions.append(
                        {action: {'index': column_index, 'alias': alias_name}})
        if actions:
            es.indices.update_aliases({'actions': actions})

    @classmethod
    def es_check_mapping(cls, index, doc_type, index_date):
        es = get_es_instance()
        if not es.indices.exists_type(index, doc_type):
            cls.es_put_mappings(date_from=index_date, date_to=index_date)

    @classmethod
    def es_versioned_metas(cls, obj_id):
        es = get_es_instance()
        db_alias = get_setting('DB_MASTER_ALIAS')
        doc_type = cls.es_class_meta_data()[1]
        # get dates for all time index columns
        column_index_map = cls._es_get_option('column_index_map')
        column_dates = cls.objects.using(db_alias).filter(
            pk=obj_id).values(*column_index_map.keys())[:1][0]
        # get versions for time indexed columns
        column_metas = []
        for column, base_index_name in column_index_map.items():
            index_date = column_dates.get(column)
            if index_date is None:
                continue
            index = format_timed_index(base_index_name, index_date)
            # check if index exists and create when needed
            cls.es_check_mapping(index, doc_type, index_date)
            # compose meta
            meta = {'index': index, 'doc_type': doc_type, 'id': obj_id}
            # check version
            try:
                old_doc = es.get(_source=False, **meta)
                meta['version'] = old_doc['_version']
            except NotFoundError:
                pass
            column_metas.append(meta)
        return column_metas

    def es_meta_data(self, bulk_keys=False, column=None, index_date=None):
        """ Overrides the es_meta_data of ESIndexedModelMixin. Takes the retval
        and replaces the generic index with a specific times index as computed
        by es_get_index.

        Args:
            bulk_keys (bool): whether to return in a format applicable for
                bulk indexing
            column (str): column name
            index_date (date): index date

        Returns:
            dict: Meta data
        """
        retval = super(ESTimeIndexedModelMixin, self).es_meta_data(bulk_keys)

        if not bulk_keys:
            retval['index'] = self.es_get_index(column, index_date)
        else:
            retval['_index'] = self.es_get_index(column, index_date)

        return retval

    def es_get_index(self, column=None, index_date=None):
        """ Find the index to use for this record.
        If column is not given, it uses the one in default_column_index param.
        If index_date is not given, it checks the value in the given column.

        Args:
            column (str): name of the column to index
            index_date (date): date for the dated index

        Returns:
            str: timed index name
        """
        cls = self.__class__
        if column is None:
            column = cls._es_get_option('default_column_index')

        column_index_map = cls._es_get_option('column_index_map')
        base_index_name = column_index_map[column]
        base_index_name = apply_index_prefix(base_index_name)

        # If date is not given, take it from the object
        if index_date is None:
            index_date = getattr(self, column, None)

        # If we don't have value in obj_date, that usually means an object
        # is not selected In that case, we return the index as a template.
        if index_date is None:
            return None

        return format_timed_index(base_index_name, index_date)

    def es_get_bulk_index_actions(self):
        """ Prepare and return actions for the bulk indexing operation.
        These actions are composed of the document and meta data.

        Returns:
            list: List of actions
        """
        cls = self.__class__
        doc = self.es_get_doc()
        column_index_map = cls._es_get_option('column_index_map')

        actions = []
        for column in column_index_map:
            index_date = getattr(self, column, None)
            if index_date is None:
                continue
            meta = self.es_meta_data(
                bulk_keys=True, column=column, index_date=index_date)
            action = dict(doc)
            action.update(meta)
            actions.append(action)

        return actions

    def es_index_me(self):
        """ Index the record into ES. This function loop over all defined
        column indices, checks the date and pushes the document into the
        appropriate index.

        Returns:
            dict: result of the last column index operation
        """
        cls = self.__class__
        es = get_es_instance()
        doc = self.es_get_doc()
        column_index_map = cls._es_get_option('column_index_map')
        for column in column_index_map:
            index_date = getattr(self, column, None)
            if index_date is None:
                continue
            meta = self.es_meta_data(column=column, index_date=index_date)
            if not es.indices.exists_type(meta['index'], meta['doc_type']):
                cls.es_put_mappings(date_from=index_date, date_to=index_date)
            cls.es_meta_include_old_version(meta)
            result = es.index(body=doc, **meta)
        return result


@receiver(post_save)
def index_after_save(sender, instance, **kwargs):
    if not getattr(instance, '_ELASTICSEARCH_ENABLED', False):
        return
    default = get_setting('ES_AUTO_INDEX')
    if sender._es_get_option('auto_index', default):
        method = instance.es_index_me
        if get_setting('ES_AUTO_INDEX_IN_CELERY'):
            method = instance.es_index_me_async
        try:
            if hasattr(connection, 'on_commit'):
                connection.on_commit(method)
            else:
                logger.warning('Connection on_commit is not supported in'
                               'this backend engine.')
                method()
        except RequestError as ex:
            logger.exception(ex.info)
        except Exception:
            logger.exception(
                'Unable to schedule indexing for %s %s',
                instance.__class__,
                instance.pk,
            )


@receiver(post_delete)
def remove_after_delete(sender, instance, **kwargs):
    if not getattr(instance, '_ELASTICSEARCH_ENABLED', False):
        return

    default = get_setting('ES_AUTO_INDEX')
    if sender._es_get_option('auto_index', default):
        method = instance.es_delete_me
        if get_setting('ES_AUTO_INDEX_IN_CELERY'):
            method = instance.es_delete_me_async
        try:
            method()
        except RequestError as ex:
            logger.exception(ex.info)
        except Exception:
            logger.exception(
                'Unable to schedule indexing for %s %s',
                instance.__class__,
                instance.pk,
            )
