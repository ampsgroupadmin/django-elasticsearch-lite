from builtins import range
import logging
from queue import Queue
import threading
import time

from django.conf import settings

from elasticsearch2.helpers import bulk

from .mixins import get_es_instance
from .module_settings import get_setting


class BulkIndexer(object):

    def __init__(self, qset, workers=5, step_size=1000):
        self.qset = qset
        self.step_size = step_size

        # I assume that the number of elements in each column index will be
        # about the same. If not, the will be some waste,
        # but this is a one-time operation,
        # and the step_size worked as a limit anyway.
        model = self.qset.model
        if getattr(model, '_ELASTICSEARCH_TIME_INDEXED', False):
            self.step_size //= len(model._es_get_option('column_index_map'))

        self.queue = Queue(maxsize=1000)
        self.workers = workers
        self.es = get_es_instance()
        self.worker_processing_times = []
        self.processed = 0
        self.logger = logging.getLogger('elasticsearch_lite.bulk')
        self.reporter_tick = 60
        self._collected = False

    def index_batch(self, qset):
        actions = []
        for item in qset:
            # self.logger.debug(item)
            data = item.es_get_doc()
            if not getattr(item, '_ELASTICSEARCH_TIME_INDEXED', False):
                meta = item.es_meta_data(bulk_keys=True)
                # self.logger.debug("Doc: %s" % data)
                # self.logger.debug("Meta: %s" % meta)
                data.update(meta)
                actions.append(data)
            else:
                actions += item.es_get_bulk_index_actions()

        self.logger.debug('Number of actions: %s', len(actions))

        _, errors = bulk(self.es, actions, raise_on_error=False)
        for error in errors:
            self.logger.error("Indexing failed: %s", error)

    def pre_process_work_item(self, item):
        """
        Pre process partial queryset before indexing method takes over
        """
        return item

    def worker(self):
        while True:
            item = self.queue.get()
            if item is StopIteration:
                self.queue.task_done()
                break

            time1 = time.time()
            try:
                processed_item = self.pre_process_work_item(item)
                self.index_batch(processed_item)
            except Exception:
                self.logger.exception("Exception in worker.")
            finally:
                with threading.Lock():
                    self.processed += 1
                self.queue.task_done()
                time2 = time.time() - time1
                self.worker_processing_times.append(time2)

        # Clean up (leaving open connection locks tests as in
        # https://code.djangoproject.com/ticket/18984#comment:13)
        from django.db import connection
        connection.close()

    def reporter(self, start_time):
        while True:
            if self.queue.empty() and self._collected:
                break
            if not self.processed:
                time.sleep(5)
                continue

            duration = (time.time() - start_time)
            per_task = duration / self.processed
            eta = self.queue.qsize() * per_task
            self.logger.info(
                "Done: %d. Left: %d%s. Avg per task: %s. Est ETA: %s min.",
                self.processed, self.queue.qsize(),
                '' if self._collected else '+', per_task, int(eta) / 60
            )
            time.sleep(self.reporter_tick)

    def start_reporter(self):
        reporter = threading.Thread(target=self.reporter, args=(time.time(),))
        reporter.setDaemon(True)
        reporter.start()

    def enqueue_work_items(self):
        cnt = self.qset.count()

        for i in range(0, cnt, self.step_size):
            worker_qset = self.qset[i:i + self.step_size]
            self.queue.put(list(worker_qset))

    def run(self):
        model_name = getattr(self.qset.model._meta, 'model_name', None)
        model_name = model_name or getattr(
            self.qset.model._meta, 'module_name'
        )
        self.logger.info(
            "Started bulk index for %s with step = %d and %d workers",
            model_name, self.step_size, self.workers
        )

        # Start workers
        self.start_reporter()
        self.logger.debug("Starting workers")
        workers = []
        for x in range(self.workers):
            thread = threading.Thread(target=self.worker)
            thread.setDaemon(True)
            thread.start()
            workers.append(thread)

        # Enqueue work pieces
        self.enqueue_work_items()

        # Put end markers
        self._collected = True
        for i in range(self.workers):
            self.queue.put(StopIteration)

        self.logger.debug("Waiting for workers (in queue: %s)",
                          self.queue.qsize())
        self.queue.join()
        self.logger.debug("Finished with %s", model_name)


class SpeedupBulkIndexer(BulkIndexer):
    """
    Indexer class to more efficiently fetch data from DB

    It ignores filter/except/limit rules defined in queryset so it's
    applicable only if you want the whole table indexed.

    It fetches object ids, puts them on a queue. Workers fetch complete objects
    from db (using IN so be careful with step_size) and index them.
    Should work nicely on MySQL and PostgreSQL.
    """

    def pre_process_work_item(self, item):
        # Fetch only row ids first and then filter on those ids to get whole
        # data - around 10 times faster than fetching results straight away
        # with LIMIT/OFFSET
        model = self.qset.model
        return model.objects.filter(pk__in=item).order_by()

    def _get_next_ids(self, last_id):
        model = self.qset.model
        pk = model._meta.pk.column
        sql = 'SELECT `%s` from `%s` WHERE `%s` > %%s ORDER BY `%s`' \
            ' LIMIT %%s' % (pk, model._meta.db_table, pk, pk)
        params = (last_id, self.step_size)
        return [x.id for x in self.qset.model.objects.raw(sql, params)]

    def enqueue_work_items(self):
        # This count will probably take loong time on PG with big tables
        # Monkey patch your queryset if you don't need it
        # self.logger.info("Found %d items to be indexed, step size: %s",
        #                  self.qset.count(), self.step_size)
        last_id = 0
        while True:
            ids = self._get_next_ids(last_id)
            if ids:
                self.queue.put(ids)
                last_id = ids[-1]
            else:
                break


def bulk_index_async(qset, workers=5, step_size=None, klass=None):
    """Index objects from given queryset using bulk API."""
    if step_size is None:
        step_size = get_setting('ES_BULK_SEGMENT_SIZE')
    if klass is None:
        klass = BulkIndexer

    indexer = klass(qset, workers, step_size)
    indexer.run()


def bulk_index(qset, raise_errors=settings.DEBUG):
    """Index objects from given queryset using bulk API."""
    step = get_setting('ES_BULK_SEGMENT_SIZE')
    es = get_es_instance()
    cnt = qset.count()
    for i in range(0, cnt, step):
        actions = []
        for item in qset[i:i + step]:
            data = item.es_get_doc()
            meta = item.es_meta_data(bulk_keys=True)
            data.update(meta)
            actions.append(data)
        bulk(es, actions, raise_on_error=raise_errors)
