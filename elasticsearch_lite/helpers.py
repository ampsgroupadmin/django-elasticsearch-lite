from __future__ import absolute_import

from datetime import date, datetime

from django.apps import apps

from dateutil.relativedelta import relativedelta
from dateutil.rrule import MONTHLY, rrule

from .es_proxy import ElasticsearchProxy, apply_index_prefix
from .module_settings import get_setting

_es_instance = None


class IncompleteESResponse(Exception):
    """Raise when not all shards respond successful"""


class EmptyTimedIndicesResult(Exception):
    """Raise by get_indices_in_range when the result is empty"""


def get_es_instance(force_new=False):
    global _es_instance
    if _es_instance is None or force_new:
        _es_instance = ElasticsearchProxy(
            get_setting('ES_HOSTS'),
            **get_setting('ES_KWARGS')
        )
    return _es_instance


def format_timed_index(base_name, index_date):
    obj_date_str = index_date.strftime("%Y-%m")

    index_prefix = get_setting('ES_INDEX_PREFIX')
    if index_prefix:
        base_name = apply_index_prefix(base_name)

    index = "%s_%s" % (base_name, obj_date_str)
    return index


def get_indices_in_range(
        indices, range_start, range_end, check_index_exists=True):
    if isinstance(indices, str):
        indices = [indices]

    if isinstance(range_start, str):
        range_start = datetime.strptime(range_start, '%Y-%m-%d')
    if isinstance(range_end, str):
        range_end = datetime.strptime(range_end, '%Y-%m-%d')

    es = get_es_instance()
    timed_indices = []
    for index_base in indices:
        for dt in rrule(MONTHLY, dtstart=range_start.replace(day=1),
                        until=range_end.replace(day=3)):
            index_date = dt.date().replace(day=1)
            timed_index = format_timed_index(index_base, index_date)

            if check_index_exists and not es.indices.exists(timed_index):
                continue

            timed_indices.append(timed_index)

    if not timed_indices:
        raise EmptyTimedIndicesResult(
            'Empty indices list for %s in range %s to %s' % (
                indices, range_start, range_end))

    return timed_indices


def get_registered(app_label=None, index_type=None):
    """
    Return all model classes registered for indexing.

    if app_label is specified only models from given app are returned.
    """
    all_models = []
    for m in apps.get_models():
        is_time_indexed = getattr(m, '_ELASTICSEARCH_TIME_INDEXED', False)
        if not getattr(m, '_ELASTICSEARCH_ENABLED', False):
            continue
        if index_type is None:
            all_models.append(m)
        elif index_type == "app" and not is_time_indexed:
            all_models.append(m)
        elif index_type == "time" and is_time_indexed:
            all_models.append(m)

    if app_label is not None:
        all_models = [m for m in all_models if m._meta.app_label == app_label]

    return all_models


def get_index_date(index_name):
    try:
        index_root, date_str = index_name.rsplit("_", 1)
    except ValueError:
        index_root = index_name
        date_str = ""

    # Check if this is a timed index
    try:
        index_date = datetime.strptime(date_str, "%Y-%m").date()
    except ValueError:
        index_date = None

    return index_root, index_date


def index_in_date_range(index_date, timediff_from, timediff_until):
    this_month = date.today() + relativedelta(day=1)

    if timediff_from is not None:
        timediff_from = relativedelta(months=timediff_from)
        in_range_bottom = index_date > this_month - timediff_from
    else:
        in_range_bottom = True

    if timediff_until is not None:
        timediff_until = relativedelta(months=timediff_until)
        in_range_top = index_date < this_month - timediff_until
    else:
        in_range_top = True

    return in_range_bottom and in_range_top


def check_es_response(result):
    """Check if all shards response successful"""
    if '_shards' in result and result['_shards'].get('failed', 0) > 0:
        raise IncompleteESResponse('Shards failures: %s' %
                                   result['_shards']['failures'])
    return True


def get_existing_column_indices(column):
    es = get_es_instance()
    # We're looking for timed indices with names in the format
    # [column]_[year]-[month]
    indices = filter(
        lambda x: x.startswith('%s_2' % column),
        es.indices.get_aliases().keys())
    return indices
