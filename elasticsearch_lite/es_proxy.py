import weakref

from elasticsearch2 import Elasticsearch, Transport
from elasticsearch2.client import IndicesClient, query_params

from .module_settings import get_setting


def apply_index_prefix(index):
    index_prefix = get_setting('ES_INDEX_PREFIX')
    if not index_prefix:
        return index

    if isinstance(index, list):
        new_index = []
        for idx in index:
            if idx.startswith(index_prefix):
                new_index.append(idx)
            else:
                new_index.append("%s_%s" % (index_prefix, idx))
        return new_index

    if index.startswith(index_prefix):
        return index

    return "%s_%s" % (index_prefix, index)


def apply_alias_prefix(alias_mapping):
    index_prefix = get_setting('ES_INDEX_PREFIX')
    if not index_prefix:
        return alias_mapping

    tmp_alias_mapping = {}
    for alias, alias_data in alias_mapping.items():
        alias = apply_index_prefix(alias)

        tmp_alias_mapping[alias] = alias_data

    return tmp_alias_mapping


class IndicesClientProxy(IndicesClient):
    @query_params('analyzer', 'char_filters', 'field', 'filters', 'format',
                  'prefer_local', 'text', 'tokenizer')
    def analyze(self, index=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).analyze(
            index=index, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'force',
                  'ignore_unavailable', 'operation_threading')
    def refresh(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).refresh(
            index=index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'force',
                  'ignore_unavailable', 'wait_if_ongoing')
    def flush(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).flush(
            index=index,
            params=params,
        )

    @query_params('master_timeout', 'timeout', 'update_all_types')
    def create(self, index, body=None, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).create(
            index, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'flat_settings',
                  'human', 'ignore_unavailable', 'local')
    def get(self, index, feature=None, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get(
            index, feature=feature, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'master_timeout', 'timeout')
    def open(self, index, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).open(index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'master_timeout', 'timeout')
    def close(self, index, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).close(index, params=params)

    @query_params('master_timeout', 'timeout')
    def delete(self, index, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).delete(index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def exists(self, index, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).exists(index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def exists_type(self, index, doc_type, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).exists_type(
            index, doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'master_timeout', 'timeout', 'update_all_types')
    def put_mapping(self, doc_type, body, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).put_mapping(
            doc_type, body, index=index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def get_mapping(self, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_mapping(
            index=index, doc_type=doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'include_defaults', 'local')
    def get_field_mapping(
            self, fields, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_field_mapping(
            fields, index=index, doc_type=doc_type, params=params)

    @query_params('master_timeout', 'timeout')
    def put_alias(self, index, name, body=None, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).put_alias(
            index, name, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def exists_alias(self, index=None, name=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).exists_alias(
            index=index, name=name, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def get_alias(self, index=None, name=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_alias(
            index=index, name=name, params=params)

    @query_params('local', 'timeout')
    def get_aliases(self, index=None, name=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_aliases(
            index=index, name=name, params=params)

    @query_params('master_timeout', 'timeout')
    def delete_alias(self, index, name, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).delete_alias(
            index,
            name,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'flat_settings',
                  'human', 'ignore_unavailable', 'local')
    def get_settings(self, index=None, name=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_settings(
            index=index, name=name, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'flat_settings',
                  'ignore_unavailable', 'master_timeout')
    def put_settings(self, body, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).put_settings(
            body, index=index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'master_timeout', 'request_cache')
    def put_warmer(self, name, body, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).put_warmer(
            name, body, index=index, doc_type=doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local')
    def get_warmer(self, index=None, doc_type=None, name=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_warmer(
            index=index, doc_type=doc_type, name=name, params=params)

    @query_params('master_timeout')
    def delete_warmer(self, index, name, params=None):
        index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).delete_warmer(
            index, name, params=params)

    @query_params('completion_fields', 'fielddata_fields', 'fields', 'groups',
                  'human', 'level', 'types')
    def stats(self, index=None, metric=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).stats(
            index=index, metric=metric, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'human',
                  'ignore_unavailable', 'operation_threading')
    def segments(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).segments(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'flush',
                  'ignore_unavailable', 'max_num_segments',
                  'only_expunge_deletes',
                  'operation_threading', 'wait_for_merge')
    def optimize(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).optimize(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'analyze_wildcard', 'analyzer',
                  'default_operator', 'df', 'expand_wildcards', 'explain',
                  'ignore_unavailable', 'lenient', 'lowercase_expanded_terms',
                  'operation_threading', 'q', 'rewrite')
    def validate_query(
            self, index=None, doc_type=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).validate_query(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'field_data',
                  'fielddata', 'fields', 'ignore_unavailable', 'query',
                  'recycler',
                  'request')
    def clear_cache(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).clear_cache(
            index=index,
            params=params,
        )

    @query_params('active_only', 'detailed', 'human')
    def recovery(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).recovery(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'only_ancient_segments', 'wait_for_completion')
    def upgrade(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).upgrade(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'human',
                  'ignore_unavailable')
    def get_upgrade(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).get_upgrade(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable')
    def flush_synced(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).flush_synced(
            index=index,
            params=params,
        )

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'operation_threading', 'status')
    def shard_stores(self, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(IndicesClientProxy, self).shard_stores(
            index=index,
            params=params,
        )


class ElasticsearchProxy(Elasticsearch):
    def __init__(self, hosts=None, transport_class=Transport, **kwargs):
        super(ElasticsearchProxy, self).__init__(hosts, transport_class,
                                                 **kwargs)
        self.indices = IndicesClientProxy(weakref.proxy(self))

    @query_params('consistency', 'parent', 'refresh', 'routing',
                  'timeout', 'timestamp', 'ttl', 'version', 'version_type')
    def create(self, index, doc_type, body, id=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).create(
            index, doc_type, body, id=id, params=params)

    @query_params('consistency', 'op_type', 'parent', 'refresh', 'routing',
                  'timeout', 'timestamp', 'ttl', 'version', 'version_type')
    def index(self, index, doc_type, body, id=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).index(
            index, doc_type, body, id=id, params=params)

    @query_params('parent', 'preference', 'realtime', 'refresh', 'routing')
    def exists(self, index, doc_type, id, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).exists(
            index, doc_type, id, params=params)

    @query_params('_source', '_source_exclude', '_source_include', 'fields',
                  'parent', 'preference', 'realtime', 'refresh', 'routing',
                  'version', 'version_type')
    def get(self, index, id, doc_type='_all', params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).get(
            index, id, doc_type=doc_type, params=params)

    @query_params('_source', '_source_exclude', '_source_include', 'parent',
                  'preference', 'realtime', 'refresh', 'routing', 'version',
                  'version_type')
    def get_source(self, index, doc_type, id, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).get_source(
            index, doc_type, id, params=params)

    @query_params('_source', '_source_exclude', '_source_include', 'fields',
                  'preference', 'realtime', 'refresh')
    def mget(self, body, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).mget(
            body, index=index, doc_type=doc_type, params=params)

    @query_params('consistency', 'detect_noop', 'fields', 'lang', 'parent',
                  'refresh', 'retry_on_conflict', 'routing', 'script',
                  'script_id',
                  'scripted_upsert', 'timeout', 'timestamp', 'ttl', 'version',
                  'version_type')
    def update(self, index, doc_type, id, body=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).update(
            index, doc_type, id, body=body, params=params)

    @query_params('_source', '_source_exclude', '_source_include',
                  'allow_no_indices', 'analyze_wildcard', 'analyzer',
                  'default_operator',
                  'df', 'expand_wildcards', 'explain', 'fielddata_fields',
                  'fields',
                  'from_', 'ignore_unavailable', 'lenient',
                  'lowercase_expanded_terms',
                  'preference', 'q', 'request_cache', 'routing', 'scroll',
                  'search_type',
                  'size', 'sort', 'stats', 'suggest_field', 'suggest_mode',
                  'suggest_size', 'suggest_text', 'terminate_after', 'timeout',
                  'track_scores', 'version')
    def search(self, index=None, doc_type=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).search(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'local', 'preference', 'routing')
    def search_shards(self, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).search_shards(
            index=index, doc_type=doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'preference', 'routing', 'scroll', 'search_type')
    def search_template(self, index=None, doc_type=None, body=None,
                        params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).search_template(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('_source', '_source_exclude', '_source_include',
                  'analyze_wildcard', 'analyzer', 'default_operator', 'df',
                  'fields', 'lenient', 'lowercase_expanded_terms', 'parent',
                  'preference', 'q', 'routing')
    def explain(self, index, doc_type, id, body=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).explain(
            index, doc_type, id, body=body, params=params)

    @query_params('consistency', 'parent', 'refresh', 'routing', 'timeout',
                  'version', 'version_type')
    def delete(self, index, doc_type, id, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).delete(
            index, doc_type, id, params=params)

    @query_params('allow_no_indices', 'analyze_wildcard', 'analyzer',
                  'default_operator', 'df', 'expand_wildcards',
                  'ignore_unavailable',
                  'lenient', 'lowercase_expanded_terms', 'min_score',
                  'preference', 'q',
                  'routing')
    def count(self, index=None, doc_type=None, body=None, params=None):
        if index:
            apply_index_prefix(index)

        return super(ElasticsearchProxy, self).count(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('consistency', 'fields', 'refresh', 'routing', 'timeout')
    def bulk(self, body, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).bulk(
            body, index=index, doc_type=doc_type, params=params)

    @query_params('search_type')
    def msearch(self, body, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).msearch(
            body, index=index, doc_type=doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'preference', 'routing')
    def suggest(self, body, index=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).suggest(
            body, index=index, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'percolate_format', 'percolate_index',
                  'percolate_preference', 'percolate_routing',
                  'percolate_type', 'preference', 'routing', 'version',
                  'version_type')
    def percolate(self, index, doc_type, id=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).percolate(
            index, doc_type, id=id, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable')
    def mpercolate(self, body, index=None, doc_type=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).mpercolate(
            body, index=index, doc_type=doc_type, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'percolate_index', 'percolate_type', 'preference', 'routing',
                  'version',
                  'version_type')
    def count_percolate(
            self, index, doc_type, id=None, body=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).count_percolate(
            index, doc_type, id=id, body=body, params=params)

    @query_params('dfs', 'field_statistics', 'fields', 'offsets', 'parent',
                  'payloads', 'positions', 'preference', 'realtime', 'routing',
                  'term_statistics', 'version', 'version_type')
    def termvectors(self, index, doc_type, id=None, body=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).termvectors(
            index, doc_type, id=id, body=body, params=params)

    @query_params('field_statistics', 'fields', 'ids', 'offsets', 'parent',
                  'payloads', 'positions', 'preference', 'realtime', 'routing',
                  'term_statistics', 'version', 'version_type')
    def mtermvectors(self, index=None, doc_type=None, body=None, params=None):
        index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).mtermvectors(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('allow_no_indices', 'analyze_wildcard', 'analyzer',
                  'default_operator', 'df', 'expand_wildcards',
                  'ignore_unavailable',
                  'lenient', 'lowercase_expanded_terms', 'min_score',
                  'preference', 'q',
                  'routing')
    def search_exists(self, index=None, doc_type=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).search_exists(
            index=index, doc_type=doc_type, body=body, params=params)

    @query_params('allow_no_indices', 'expand_wildcards', 'fields',
                  'ignore_unavailable', 'level')
    def field_stats(self, index=None, body=None, params=None):
        if index:
            index = apply_index_prefix(index)

        return super(ElasticsearchProxy, self).field_stats(
            index=index, body=body, params=params)
