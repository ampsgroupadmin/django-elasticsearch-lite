=========================
Django Elasticsearch Lite
=========================

Simple no-frills elasticsearch integration for django models. It allows to quickly index your model data with minimal effort. There are no filtering capabilities whatsoever. It's meant for use cases where you just need to index your models and then query the data with Query DSL.

Installation and configuration
------------------------------

1. Install the app using your preferred method (alongside django, official elasticsearch module and celery) and add to INSTALLED_APPS

2. Configure models to be indexed as being descendants of ESIndexedModelMixin, eg.:

.. sourcecode:: python

        class ExampleModel(models.Model, ESIndexedModelMixin):
            name = models.CharField('Name', max_length=100)

3. Optionally fine tune some default settings

.. sourcecode:: python

        class ExampleModel(models.Model, ESIndexedModelMixin):
            name = models.CharField('Name', max_length=100)

            class ElasticSearch:
                # Non standard mappings for selected fields
                mappings = {
                    'name': {
                        "type": "string",
                        "index": "not_analyzed",
                    },
                }
                # By default index name is the app label here you can change it
                doc_index = 'other_index'
                # By default document type is the model name here you can change it
                doc_type = 'other_type'
                # Field which will be set as elasticsearch's document id ('pk' by
                # default)
                id_field = 'name'
                auto_index = False  # Do not index automatically on save
                include = ('included', )  # Index only selected fields
                exclude = ('name', )  # Index all fields but these
                # Configure parent documents
                parent_field = 'parent'
                parent_type = 'parent_class'

4. Optional settings

.. sourcecode:: python

        ES_HOSTS = [{host:'yourhostname', port: 9200}] # localhost by default
        ES_AUTO_INDEX = True  # Automatially index emabled models on save
        ES_AUTO_INDEX_IN_CELERY = False  # Use celery for the above process
        # Number of documents to send in single query when indexing models in bulk
        ES_BULK_SEGMENT_SIZE = 1000

5. Commands

* django-admin.py es_mappings APP_LABEL - show or install mappings for given app
* django-admin.py es_index APP_LABEL MODEL_NAME - bulk index given model

6. Index ManyToMany field in atomic block requires instalation django-transaction-hooks(details: https://django-transaction-hooks.readthedocs.io/en/latest/). Works for Django<1.9.
   Instance for mysql database:: 

.. sourcecode:: python
    DATABASES = {
    'default': {
        'ENGINE': 'transaction_hooks.backends.mysql',
        'NAME': 'foo',
        },
    }
